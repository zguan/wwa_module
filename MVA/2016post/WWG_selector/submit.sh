nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/HGC16.root -y 2016 > hgc.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/HGU16.root -y 2016 > hgu.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/HGD16.root -y 2016 > hgd.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/HGS16.root -y 2016 > hgs.log 2>&1 &

nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/WWG_emu16.root -y 2016 > wwg.log 2>&1 &
