nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/HGC16pre.root -y 2016pre > hgc.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/HGU16pre.root -y 2016pre > hgu.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/HGD16pre.root -y 2016pre > hgd.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/HGS16pre.root -y 2016pre > hgs.log 2>&1 &

nohup python WWG_postproc.py -f ~/cms/mva002/2016pre/2016premva002/WWG_emu16pre.root -y 2016pre > wwg.log 2>&1 &
