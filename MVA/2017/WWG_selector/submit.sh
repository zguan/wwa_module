nohup python WWG_postproc.py -f ~/cms/mva002/2018/HGU17.root -y 2017 > hgu.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2018/HGD17.root -y 2017 > hgd.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2018/HGS17.root -y 2017 > hgs.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2018/HGC17.root -y 2017 > hgc.log 2>&1 &

nohup python WWG_postproc.py -f ~/cms/mva002/2018/WWG_emu17.root -y 2017 > wwg.log 2>&1 &
