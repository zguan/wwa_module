nohup python WWG_postproc.py -f ~/cms/mva002/2018/HGU18.root -y 2018 > hgu.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2018/HGD18.root -y 2018 > hgd.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2018/HGS18.root -y 2018 > hgs.log 2>&1 &
nohup python WWG_postproc.py -f ~/cms/mva002/2018/HGC18.root -y 2018 > hgc.log 2>&1 &

nohup python WWG_postproc.py -f ~/cms/mva002/2018/WWG_emu18.root -y 2018 > wwg.log 2>&1 &
